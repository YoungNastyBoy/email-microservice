package com.example.emailSender.model;

import java.util.List;

public class EmailWrapper {
    private List<Email> emails;

    public EmailWrapper(List<Email> emails){
        this.emails = emails;
    }

    public EmailWrapper() {
    }

    public List<Email> getEmails() {
        return emails;
    }
}
