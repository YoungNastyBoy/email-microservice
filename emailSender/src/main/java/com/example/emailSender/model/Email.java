package com.example.emailSender.model;

public class Email {
    private Long id;
    private String email;

    public Email(String email) {
        this.email = email;
    }

    public Email() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
