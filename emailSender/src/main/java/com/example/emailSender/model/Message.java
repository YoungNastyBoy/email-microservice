package com.example.emailSender.model;

import org.springframework.stereotype.Component;

@Component
public class Message {

    private String from;
    private String password;
    private String subject;
    private String content;

    public Message(String from, String subject, String content, String password) {
        this.from = from;
        this.password = password;
        this.subject = subject;
        this.content = content;
    }

    public Message() {
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
