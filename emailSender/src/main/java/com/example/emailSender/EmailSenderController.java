package com.example.emailSender;


import com.example.emailSender.model.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/sender")
public class EmailSenderController {

    @Autowired
    private SenderService service;

    @GetMapping("/send-to/all")
    public void sendEmailToAll(@RequestBody Message message){
        service.sendToAll(message);
    }

    @GetMapping("/send-to/{id}")
    public void forwardToGiven(@PathVariable Long id, @RequestBody Message message){
        service.sendToGiven(id, message);
    }
}
