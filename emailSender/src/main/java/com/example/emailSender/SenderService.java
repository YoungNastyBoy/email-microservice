package com.example.emailSender;

import com.example.emailSender.model.Email;
import com.example.emailSender.model.EmailWrapper;

import com.example.emailSender.model.Message;
import com.netflix.discovery.converters.Auto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

@Service
public class SenderService{

    private JavaMailSender mailSender;

    @Autowired
    private RestTemplate restTemplate;

    public void sendToAll(Message message) {
        mailSender = getMailSender(message);
        List<String> emails = getEmailsFromDB();
        for(String email : emails){
            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setTo(email);
            mailMessage.setSubject(message.getSubject());
            mailMessage.setText(message.getContent());
            mailMessage.setFrom(message.getFrom());
            mailSender.send(mailMessage);
        }
    }

    public List<String> getEmailsFromDB(){
        Email[] response = restTemplate.getForObject("http://EMAILREST/emails", Email[].class);
        return  Arrays.stream(response)
                .map(email -> email.getEmail())
                .collect(Collectors.toList());

    }

    private JavaMailSender getMailSender(Message message){
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost("smtp.gmail.com");
        mailSender.setPort(587);
        mailSender.setUsername(message.getFrom());
        mailSender.setPassword(message.getPassword());
        mailSender.setProtocol(JavaMailSenderImpl.DEFAULT_PROTOCOL);

        Properties prop = mailSender.getJavaMailProperties();
        prop.put("mail.smtp.starttls.enable", "true");
        prop.put("mail.smtp.auth", "true");

        return mailSender;
    }


    public void sendToGiven(Long id, Message message) {
        String recipient = getEmailById(id);
        mailSender = getMailSender(message);
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(recipient);
        mailMessage.setSubject(message.getSubject());
        mailMessage.setText(message.getContent());
        mailMessage.setFrom(message.getFrom());
        mailSender.send(mailMessage);

    }

    private String getEmailById(Long id){
        Email response = restTemplate.getForObject("http://EMAILREST/api/emails/{id}",
                Email.class, id);
        return response.getEmail();
    }
}
