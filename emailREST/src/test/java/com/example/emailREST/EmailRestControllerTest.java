package com.example.emailREST;

import com.example.emailREST.model.Email;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.reactive.server.HttpHandlerConnector;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.client.RestTemplate;
import reactor.core.publisher.Mono;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ExtendWith(MockitoExtension.class)
class EmailRestControllerTest {

    @LocalServerPort
    private final int port = 8081;

    private RestTemplate restTemplate;
    @Mock
    private EmailService emailService;
    @InjectMocks
    private EmailRestController controller;
    private WebTestClient client;

    @BeforeEach
    public void setup(){
        restTemplate = new RestTemplate();
        MockitoAnnotations.openMocks(this);
        client = WebTestClient.bindToServer().baseUrl("http://localhost:8081/api").build();
    }

    @Test
    void shouldPassGetAllToService(){
        when(emailService.getAllEmails()).thenReturn(List.of(new Email(1L, "test@email.com")));

        assertEquals(List.of(new Email(1L, "test@email.com")), controller.getEmails());
    }
    @ParameterizedTest
    @ValueSource(longs = {2L})
    void shouldPassGetByIdToService(Long id){
        when(emailService.getEmailById(id)).thenReturn(new Email(id, "test@email.com"));

        assertEquals(new Email(id, "test@email.com"), controller.getEmails(id).getBody());
    }
    @Test
    void shouldPassPostToService(){
        Email email = new Email(3L, "posttest@email.com");
        when(emailService.createEmail(email)).thenReturn(email);

        assertEquals(email, controller.createEmail(email).getBody());
    }
    @ParameterizedTest
    @ValueSource(longs = {4L})
    void shouldPassPutToService(Long id){
        new Email(id, "putTest@email.com");
        Email email1 = new Email("newPutTest@Email.com");
        when(emailService.updateEmail(id, email1)).thenReturn(email1);

        assertEquals(new Email(4L, "newPutTest@Email.com"), controller.updateEmail(id, email1).getBody());
    }
    @ParameterizedTest
    @ValueSource(longs = {5L})
    void shouldPassDeleteToService(Long id){
        when(emailService.deleteEmail(id)).thenReturn(true);

        assertEquals(controller.deleteEmail(id).getStatusCode(), HttpStatus.OK);
    }

    // Integration testing, the post, update and delete tests are supposed to work only once,
    // since they are not safe operations and alter the state of database

    @Test
    void shouldRetrieveEmailFromDBByID(){
        Long id = 1L;
        Email emailWithId1 = client.get()
                .uri("/emails/{id}", id)
                .exchange()
                .returnResult(Email.class).getResponseBody().blockFirst();
        assertEquals("testemail@email.com", emailWithId1.getEmail());
    }

    @Test
    void shouldCreateEmail(){
        Email emailToCreate = new Email("finalTesting@email.com");
        client.post()
                .uri("/emails")
                .body(Mono.just(emailToCreate), Email.class)
                .exchange()
                .expectStatus().isCreated();
    }

    @Test
    void shouldUpdateExistingEmail(){
        Long id = 7L;
        Email emailToUpdate = new Email("finalTestClassTest@email.com");
        client.put()
                .uri("/emails/{id}", id)
                .body(Mono.just(emailToUpdate), Email.class)
                .exchange()
                .expectStatus().isCreated();
    }

    @Test
    void shouldDeleteExistingEmail(){
        Long id = 8L;
        client.delete()
                .uri("/emails/{id}", id)
                .exchange()
                .expectStatus().isOk();
    }

    @Test
    void shouldReturnNotFoundOnBadDeleteRequest(){
        Long id = 40L;
        client.delete()
                .uri("/emails/{id}", id)
                .exchange()
                .expectStatus().isNotFound();
    }
    @Test
    void shouldReturnNotFoundOnBadGetRequest(){
        Long id = 40L;
        client.get()
                .uri("/emails/{id}", id)
                .exchange()
                .expectStatus().isNotFound();
    }
    @Test
    void shouldReturnConflictOnCreateAlreadyExistingEmailRequest(){
        client.post()
                .uri("/emails/")
                .body(Mono.just(new Email("testemail@email.com")), Email.class)
                .exchange()
                .expectStatus().isEqualTo(HttpStatus.CONFLICT);
    }
    @Test
    void shouldReturnConflictOnUpdateEmailBadIdRequest(){
        Long id = 40L;
        client.put()
                .uri("/emails/{id}", id)
                .body(Mono.just(new Email("conflictTest@email.com")), Email.class)
                .exchange()
                .expectStatus().isEqualTo(HttpStatus.CONFLICT);
    }
}