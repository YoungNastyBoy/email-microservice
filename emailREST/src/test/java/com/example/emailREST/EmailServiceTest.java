package com.example.emailREST;

import com.example.emailREST.model.Email;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class EmailServiceTest {

    @InjectMocks
    public EmailService emailService = new EmailService();

    public Email email;

    @Mock
    public EmailRepository emailRepository;

    @BeforeEach
    public void init(){
        MockitoAnnotations.initMocks(this);
    }


    @Test
    void shouldCreateAnEmail(){
        email = new Email(10L, "email@email.com");
        Mockito.when(emailRepository.save(email)).thenReturn(email);

        assertEquals(email, emailService.createEmail(email));
    }

    @Test
    void shouldRejectTakenEmail(){
        email = new Email(10L, "email@email.com");
        emailRepository.save(email);
        Mockito.when(emailRepository.findAll()).thenReturn(List.of(email));
        boolean isEmailNotTaken = emailService.isEmailNotTaken(email);
        assertFalse(isEmailNotTaken);
    }

    @Test
    void shouldRejectInvalidEmail(){
        email = new Email(15L, "emailwithoutatsign.com");
        assertFalse(emailService.emailIsValid(email.getEmail()));
        email = new Email(15L, "emailwithoutdomain@");
        assertFalse(emailService.emailIsValid(email.getEmail()));
        email = new Email(15L, "@emailwithoutusername.com");
        assertFalse(emailService.emailIsValid(email.getEmail()));
    }

    @Test
    void shouldUpdateExistingEmail(){
        email = new Email(10L, "email@email.com");
        emailRepository.save(email);
        Mockito.when(emailRepository.save(email)).thenReturn(email);
        Mockito.when(emailRepository.findById(email.getId())).thenReturn(Optional.of(email));

        Email updatedEmail = emailService.updateEmail(10L, new Email("newEmail@email.com"));

        assertEquals(new Email(10L, "newEmail@email.com"), updatedEmail);
    }

    @ParameterizedTest
    @ValueSource(longs = {11L})
    void shouldReturnNullNoEmail(Long id){
        email = new Email(10L, "email@email.com");
        emailRepository.save(email);
        Mockito.when(emailRepository.save(email)).thenReturn(email);
        Mockito.when(emailRepository.findById(email.getId())).thenReturn(Optional.of(email));
        Mockito.when(emailRepository.findById(id)).thenThrow(NullPointerException.class);
        assertThrows(NullPointerException.class, () ->emailService.updateEmail(id, new Email("newEmail@email.com")));
    }

    @Test
    void shouldDeleteExistingEmail(){
        email = new Email(11L, "emailToDelete@email.com");
        emailRepository.save(email);
        Mockito.when(emailRepository.findById(email.getId())).thenReturn(Optional.of(email));
        assertTrue(emailService.deleteEmail(email.getId()));
    }





}