package com.example.emailREST;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class EmailRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmailRestApplication.class, args);
	}

}
