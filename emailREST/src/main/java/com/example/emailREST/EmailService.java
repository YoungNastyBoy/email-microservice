package com.example.emailREST;

import com.example.emailREST.model.Email;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class EmailService {

    @Autowired
    private EmailRepository emailRepository;


    public List<Email> getAllEmails() {
        return (List<Email>) emailRepository.findAll();
    }

    public Email getEmailById(Long id){
        Optional<Email> possibleEmail =  emailRepository.findById(id);
        return possibleEmail.orElse(null);
    }

    public Email createEmail(Email email) {
        if(emailIsValid(email.getEmail())){
            if(isEmailNotTaken(email)){
                return emailRepository.save(email);
            }
        }
        return null;
    }

    public boolean emailIsValid(String email) {
        String regex = "^(.+)@(.+)$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public Email updateEmail(Long id, Email newEmail) {
        Email email = getEmailById(id);
        if(email != null && emailIsValid(newEmail.getEmail()) && isEmailNotTaken(newEmail)){
            email.setEmail(newEmail.getEmail());
            return emailRepository.save(email);
        }
        return null;
    }

    public boolean deleteEmail(Long id) {
        Optional<Email> email = emailRepository.findById(id);
        if(email.isPresent()){
            emailRepository.delete(email.get());
            return true;
        }else{
            return false;
        }
    }

    public boolean isEmailNotTaken(Email email){
        if(getAllEmails() == null){
            return true;
        }else{
            List<String> emails = getAllEmails().stream()
                    .map(Email::getEmail)
                    .collect(Collectors.toList());
            if(!emails.contains(email.getEmail())){
                return true;
            }
        }

        return false;
    }

}
