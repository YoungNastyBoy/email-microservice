package com.example.emailREST;

import com.example.emailREST.model.Email;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmailRepository extends PagingAndSortingRepository<Email, Long> {

}
