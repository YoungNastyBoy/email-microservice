package com.example.emailREST;

import com.example.emailREST.model.Email;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.http.HttpResponse;
import java.util.List;

@RestController
@RequestMapping("/api")
public class EmailRestController {

    @Autowired
    private EmailService emailService;

    @GetMapping("/emails")
    public List<Email> getEmails(){
        return emailService.getAllEmails();
    }

    @GetMapping("/emails/{id}")
    public ResponseEntity<Email> getEmails(@PathVariable Long id){
        Email emailById = emailService.getEmailById(id);
        if(emailById != null){
            return new ResponseEntity<>(emailById, HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/emails")
    public ResponseEntity<Email> createEmail(@RequestBody Email email){
        Email toCreate = emailService.createEmail(email);
        if(toCreate != null){
            return new ResponseEntity<>(toCreate, HttpStatus.CREATED);
        }else{
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

    @PutMapping("/emails/{id}")
    public ResponseEntity<Email> updateEmail(@PathVariable Long id,@RequestBody Email newEmail){
        Email email = emailService.updateEmail(id, newEmail);
        if(email != null){
            return new ResponseEntity<>(email, HttpStatus.CREATED);
        }else{
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

    @DeleteMapping("/emails/{id}")
    public ResponseEntity deleteEmail(@PathVariable Long id){
        if(emailService.deleteEmail(id)){
            return new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
}
