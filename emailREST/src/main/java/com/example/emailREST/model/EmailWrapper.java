package com.example.emailREST.model;

import java.util.List;

public class EmailWrapper {

    private List<Email> emails;

    public EmailWrapper(List<Email> emails) {
        this.emails = emails;
    }

    public List<Email> getEmails() {
        return emails;
    }
}
